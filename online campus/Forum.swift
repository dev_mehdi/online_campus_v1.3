//
//  forum.swift
//  online campus
//
//  Created by mehdi on 06/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation
import UIKit

class Forum {
    
    //MARK: Properties
    var id: String = ""
    var name: String = "Forum Name"
    var description: String = "Forum Description"
    var total_topics: String = "0"
    var total_posts: String = "0"
    var last_message: String = "Last Message Date"
    
    //MARK: Inits
    
    init(id: String,name: String,description: String,total_topics: String,total_posts:String,last_message: String) {
        self.id = id
        self.name = name
        self.description = description
        self.total_posts = total_posts
        self.total_topics = total_topics
        self.last_message = last_message
    }
    init(){
    }
}

