
import UIKit
import Alamofire
import SwiftyJSON
import SDLoader
import HTMLString


class MyCourses: UITableViewController {
    let global = Global()
    @IBOutlet weak var viewTable: UITableView!
    
    
    let sdLoader = SDLoader()
    
    var codeCoursesArray = [String]()
    var coursesTitleArray = [String]()
    var adminCoursesArray = [String]()
    var imgUrlArray = [String]()
    var dbCourseArray = [String]()
    var categoryCodeArray = [String]()
    var directoryArray = [String]()
    var isCourseManagerArray = [String]()
    var languageArray = [String]()
    var sysCodeArray = [String]()
    
    var header_name = "My Courses"
    
    var items = 1
    func customization() {
        self.navigationController?.navigationBar.backItem?.title = ""
        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 30, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 30, 0)
        self.view.backgroundColor = UIColor.init(red: 228, green: 34, blue: 24, alpha: 1)
        
        //self.viewTable.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.rowHeight = UITableViewAutomaticDimension
        self.viewTable.estimatedRowHeight = 340
        
        //TODO: Register your ForumTableViewCell.xib file here:
        tableView.register(UINib(nibName:"CourseTableViewCell",bundle: nil), forCellReuseIdentifier: "courseCell")

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sdLoader.startAnimating(atView: self.view)
        codeCoursesArray = [String]()
        coursesTitleArray = [String]()
        adminCoursesArray = [String]()
        imgUrlArray = [String]()
        dbCourseArray = [String]()
        categoryCodeArray = [String]()
        directoryArray = [String]()
        isCourseManagerArray = [String]()
        languageArray = [String]()
        sysCodeArray = [String]()
        items = 1
        //print(items)
        self.MyCourses()
        //print(codeCoursesArray)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // function
    func MyCourses(){
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        if  isConnected == "true" && uid != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid]
            let urlString = "\(global.domaine)/claroline/api_mobile/mycourses.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                //print(response)
                if let json = response.result.value  as? NSArray {
                    for actor in json{
                        if let coursedetail = actor as? NSDictionary {
                            //let imgUrl = "\(global.domaine)/images/courses.png"
                            self.imgUrlArray.append("courses")
                            
                            
                            if let name = coursedetail.value(forKey: "officialCode") {
                                self.codeCoursesArray.append(name as! String)
                            }
                            if let name = coursedetail["title"] {
                                self.coursesTitleArray.append((name as! String).removingHTMLEntities)
                            }
                            if let name = coursedetail.value(forKey: "titular") {
                                self.adminCoursesArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "db") {
                                self.dbCourseArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "categoryCode") {
                                self.categoryCodeArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "directory") {
                                self.directoryArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "isCourseManager") {
                                self.isCourseManagerArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "language") {
                                self.languageArray.append(name as! String)
                            }
                            if let name = coursedetail.value(forKey: "sysCode") {
                                self.sysCodeArray.append(name as! String)
                            }
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.viewTable.reloadData()
                    })
                }
                self.items = self.codeCoursesArray.count
                self.sdLoader.stopAnimation()
            }
            
        }
        
    }
    // MARK: - Table view data source



    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
      
        return self.items
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "courseCell") as! CourseTableViewCell
            if !self.codeCoursesArray.isEmpty {
                cell.codeCourses.text = codeCoursesArray[indexPath.row]
                cell.titleCourse.text = coursesTitleArray[indexPath.row]
                cell.adminname.text = adminCoursesArray[indexPath.row]
                cell.imgView.image = UIImage.init(named: self.imgUrlArray[indexPath.row])
                cell.viewCourse.backgroundColor = UIColor(red:0.94, green:0.79, blue:0.30, alpha:1.0)
                cell.ViewProf.backgroundColor = UIColor(red:0.08, green:0.70, blue:0.47, alpha:1.0)
                cell.courseViewTitle.backgroundColor = UIColor(red:0.20, green:0.30, blue:0.36, alpha:1.0)
                
                var FOLDER_NAME = ""
                FOLDER_NAME = self.sysCodeArray[indexPath.row]
                let fileManager = FileManager.default
                // creation du dossier s'il n"existe
                if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let filePath =  tDocumentDirectory.appendingPathComponent("\(FOLDER_NAME)")
                    if !fileManager.fileExists(atPath: filePath.path) {
                        do {
                            try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                            //NSLog("Document directory is created")
                        } catch {
                            //NSLog("Couldn't create document directory")
                        }
                    }
                    //NSLog("Document directory is \(filePath)")
                }
            }
        
            return cell
    }
    
    ///for showing next detailed screen with the downloaded info
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if indexPath.row != 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "course") as! Course
                //print(codeCoursesArray)
                //print(indexPath.row)
                vc.codeCoursText = codeCoursesArray[indexPath.row]
                vc.profesorText = adminCoursesArray[indexPath.row]
                vc.titleText = coursesTitleArray[indexPath.row]
                vc.dbCourse = dbCourseArray[indexPath.row]
                vc.categoryCode = categoryCodeArray[indexPath.row]
                vc.directory = directoryArray[indexPath.row]
                vc.isCourseManager = isCourseManagerArray[indexPath.row]
                vc.language = languageArray[indexPath.row]
                vc.sysCode = sysCodeArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        //}
        
    }
 
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
