

import UIKit

class ContactController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    let menuTitles = ["+1-407-801-5140", "registrar@iulf.education"]
    let iconTitles = ["phone","mail"]
    let bgimage = "bglogo"
    var items = 3
    var lastContentOffset: CGFloat = 0.0
    
    //MARK: Methods

    func customization() {
        
        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300
        self.navigationController?.navigationBar.backItem?.title = ""
        self.tableView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.tableView.frame.width), height: CGFloat(300 + (50 * self.items)))
        User.fetchData { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            //weakSelf.user = response
            //weakSelf.items += response.playlists.count
            weakSelf.tableView.reloadData()
        }
    }
    
    // MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header", for: indexPath) as! ContactHeaderCell
            cell.backgroundImage.image = UIImage(named: bgimage)
            return cell
        case 1...self.items:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! ContactMenuCell
            cell.menuTitles.text = self.menuTitles[indexPath.row - 1]
            cell.menuIcon.image = UIImage.init(named: self.iconTitles[indexPath.row - 1])
           return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! ContactHeaderCell
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: false)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: true)
        }
    }
    
    //MARK: -  ViewController Lifecylce
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization()
    }
}

class ContactHeaderCell: UITableViewCell {
    

    @IBOutlet weak var backgroundImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.profilePic.layer.cornerRadius = 25
//        self.profilePic.clipsToBounds = true
    }
}

class ContactMenuCell: UITableViewCell {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitles: UILabel!
    
}





