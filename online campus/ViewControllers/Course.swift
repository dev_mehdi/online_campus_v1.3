
import UIKit

class Course: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var viewTable: UITableView!
    let menuTitles = ["Course Description", "Documents & Links", "Assignements", "Forums"]
    var codeCoursText : String = ""
    var profesorText : String = ""
    var titleText: String = ""
    var dbCourse : String = ""
    var categoryCode : String = ""
    var directory : String = ""
    var isCourseManager : String = ""
    var language : String = ""
    var sysCode : String = ""
    var items = 5
    var lastContentOffset: CGFloat = 0.0
    
    
    //MARK: Methods
    
    func customization() {
        self.navigationController?.navigationBar.backItem?.title = ""
        //self.viewTable.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.rowHeight = UITableViewAutomaticDimension
        self.viewTable.estimatedRowHeight = 300
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    //
    
    // MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header", for: indexPath) as! HeaderCellCourse
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            cell.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
            return cell
        case 1...4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! MenuCellCourse
            cell.menuTitles.text = self.menuTitles[indexPath.row - 1]
            cell.menuIcon.image = UIImage.init(named: self.menuTitles[indexPath.row - 1])
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! HeaderCellCourse
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            let user = UserDefaults.standard
            user.set(codeCoursText, forKey: "codeCoursText")
            user.set(profesorText,forKey: "profesorText")
            user.set(titleText,forKey: "titleText")
            user.set(dbCourse,forKey: "dbCourse")
            user.set(categoryCode,forKey: "categoryCode")
            user.set(directory,forKey: "directory")
            user.set(isCourseManager,forKey: "isCourseManager")
            user.set(language,forKey: "language")
            user.set(sysCode,forKey: "sysCode")
            user.synchronize()
            print(dbCourse)
            if indexPath.row == 1{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CourseDescription") as! CourseDescription
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if indexPath.row == 2{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "document") as! Document_links
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if indexPath.row == 3{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "assignments") as! AssignementViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if indexPath.row == 4{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "forum") as! ForumViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
           
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 120
        case 1...:
            return 50
        default:
            return 120
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization();

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

}


class HeaderCellCourse: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profesor: UILabel!
    @IBOutlet weak var codeCourse: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.profilePic.layer.cornerRadius = 25
//        self.profilePic.clipsToBounds = true
    }
}

//class HeaderCell: UITableViewCell {
//
//    @IBOutlet weak var name: UILabel!
//    @IBOutlet weak var profilePic: UIImageView!
//    @IBOutlet weak var backgroundImage: UIImageView!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.profilePic.layer.cornerRadius = 25
//        self.profilePic.clipsToBounds = true
//    }
//}

class MenuCellCourse: UITableViewCell {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitles: UILabel!
    
}


