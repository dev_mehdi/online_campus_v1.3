

import UIKit

class DetailCoursesController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    var sysCodeCoursString = String()
    var titleCoursString = String()
    var adminCoursString = String()
    
    var menu = ["Course Description","Announcement","Document and links","Assignement","chat"]
    
    @IBOutlet weak var codeCours: UILabel!
    @IBOutlet weak var courstitle: UILabel!
    @IBOutlet weak var adminCours: UILabel!
    @IBOutlet weak var menuViewTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backItem?.title = ""
        updateUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellD = tableView.dequeueReusableCell(withIdentifier: "cellD") as! MenuTableViewCell
        cellD.menuItem.text = menu[indexPath.row]
        return cellD
    }

    ///for showing next detailed screen with the downloaded info
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    
//    }
    
    
    func updateUI() {
        self.adminCours.text = adminCoursString
        self.courstitle.text = titleCoursString
        self.codeCours.text = sysCodeCoursString
        OperationQueue.main.addOperation({
            self.menuViewTable.reloadData()
           
        })
//        let imgURL = URL(string:imageString)
//        
//        let data = NSData(contentsOf: (imgURL)!)
//        self.imageView.image = UIImage(data: data! as Data)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
