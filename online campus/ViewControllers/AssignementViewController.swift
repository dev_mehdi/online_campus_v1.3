

import UIKit
import Alamofire
import MBProgressHUD
import WebKit

class AssignementViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
        
        let global = Global()
        
    @IBOutlet weak var viewTableDoc: UITableView!
    //let menuTitles = ["Cours Description","Assignements", "Chat"]
    //course  attribute
    var codeCoursText : String = UserDefaults.standard.string(forKey: "codeCoursText")!
    var profesorText : String = UserDefaults.standard.string(forKey: "profesorText")!
    var titleText: String = UserDefaults.standard.string(forKey: "titleText")!
    var dbCourse : String = UserDefaults.standard.string(forKey: "dbCourse")!
    var categoryCode : String = UserDefaults.standard.string(forKey: "categoryCode")!
    var directory : String = UserDefaults.standard.string(forKey: "directory")!
    var isCourseManager : String = UserDefaults.standard.string(forKey: "isCourseManager")!
    var language : String = UserDefaults.standard.string(forKey: "language")!
    var sysCode : String = UserDefaults.standard.string(forKey: "sysCode")!
    //file attribute
    var titleArray = [String]()
    var visibilityArray = [String]()
    var descriptionArray = [String]()
    var idArray = [String]()
    var typeArray = [String]()
    var startDateArray = [String]()
    var endDateArray = [String]()
    var lastContentOffset: CGFloat = 0.0
    let header_i = 1
    let heightWebView: CGFloat = 100
    
    
    var items = 0
    var header_name = "Assignments"
    func customization() {
        self.navigationController?.navigationBar.backItem?.title = ""
        //self.viewTable.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTableDoc.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTableDoc.rowHeight = UITableViewAutomaticDimension
        //self.viewTableDoc.estimatedRowHeight = 340
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.MyAssignment()
        self.customization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function
    func MyAssignment(){
        var uid : String
        uid = UserDefaults.standard.string(forKey: "uid")!
        print(self.sysCode)
        print(self.dbCourse)
        var isConnected : String
        isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        if  isConnected == "true" && uid != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"cid" : sysCode,"dbtable" : dbCourse]
            let urlString = "\(global.domaine)/claroline/api_mobile/work_api.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                print(response);
                if let json = response.result.value  as? NSArray {
                    
                    for js in json{
                        if let ass = js as? NSDictionary {
                            if let id = ass.value(forKey: "id") {
                                self.idArray.append(id as! String)
                            }
                            if let title = ass.value(forKey: "title") {
                                self.titleArray.append(title as! String)
                            }
                            if let description = ass.value(forKey: "description") {
                                self.descriptionArray.append(description as! String)
                            }
                            if let startDate = ass.value(forKey: "startDate") {
                                self.startDateArray.append(startDate as! String)
                            }
                            if let endDate = ass.value(forKey: "endDate") {
                                self.endDateArray.append(endDate as! String)
                            }
                            if let type = ass.value(forKey: "type") {
                                self.typeArray.append(type as! String)
                            }
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.viewTableDoc.reloadData()
                    })
                }
                self.items = self.titleArray.count + self.header_i
                //print(self.items)
            }
            
        }
        
    }
    
    func reload(url: String){
        self.MyAssignment()
        
    }
    // MARK: - Table view data source
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerAss", for: indexPath) as! HeaderAssignmentCell
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            cell.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
            return cell
        case 1...:
            let cell = tableView.dequeueReusableCell(withIdentifier: "contentCellAss", for: indexPath) as! ContentCellAssignment
            cell.title.text = self.titleArray[indexPath.row - self.header_i]
            cell.startDate.text = "Start Date : \(self.startDateArray[indexPath.row - self.header_i])"
            cell.endDate.text = "End Date : \(self.endDateArray[indexPath.row - self.header_i])"
            
            let htmlCode = "<html><head><title></title><meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'></head> <body>"+self.descriptionArray[indexPath.row - self.header_i]+"</body></html>"
            cell.webView.loadHTMLString(htmlCode, baseURL: nil)
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerAss", for: indexPath) as! HeaderAssignmentCell
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            
            return cell
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
            case 0:
                return 120
            case 1...:
                return 300
            default:
                return 120
        }
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
}
    
    // Header
    class HeaderAssignmentCell: UITableViewCell {
        
        @IBOutlet weak var name: UILabel!
        @IBOutlet weak var profesor: UILabel!
        @IBOutlet weak var codeCourse: UILabel!        
        override func awakeFromNib() {
            super.awakeFromNib()
            //        self.profilePic.layer.cornerRadius = 25
            //        self.profilePic.clipsToBounds = true
        }
    }


// Header
class ContentCellAssignment: UITableViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.profilePic.layer.cornerRadius = 25
        //        self.profilePic.clipsToBounds = true
    }
}

