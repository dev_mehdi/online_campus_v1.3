

import UIKit
import WebKit

class ReaderViewController: UIViewController {

    //@IBOutlet weak var webView: UIWebView!
    
   
    @IBOutlet weak var wb: UIWebView!
    var urlString:String! = ""
    var url:URL? = nil
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        let request = URLRequest(url: self.url!)
        self.wb.loadRequest(request)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
