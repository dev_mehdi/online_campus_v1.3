
import UIKit
import Alamofire
import WebKit

class CourseDescription: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    let global = Global()
    
    @IBOutlet weak var viewTable: UITableView!
    var codeCoursText : String = UserDefaults.standard.string(forKey: "codeCoursText")!
    var profesorText : String = UserDefaults.standard.string(forKey: "profesorText")!
    var titleText: String = UserDefaults.standard.string(forKey: "titleText")!
    var dbCourse : String = UserDefaults.standard.string(forKey: "dbCourse")!
    var categoryCode : String = UserDefaults.standard.string(forKey: "categoryCode")!
    var directory : String = UserDefaults.standard.string(forKey: "directory")!
    var isCourseManager : String = UserDefaults.standard.string(forKey: "isCourseManager")!
    var language : String = UserDefaults.standard.string(forKey: "language")!
    var sysCode : String = UserDefaults.standard.string(forKey: "sysCode")!
    var items = 1
    var lastContentOffset: CGFloat = 0.0
    var backgroundImage = ""
    var titleArray = [String]()
    var contentArray = [String]()
    
    //override Methods
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        self.customization()
        self.DescriptionCourse()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Methods
    
    func customization() {
        //self.viewTable.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTable.rowHeight = UITableViewAutomaticDimension
        self.viewTable.estimatedRowHeight = 300
        self.navigationController?.navigationBar.backItem?.title = ""
        
    }
    
    // function
    func DescriptionCourse(){
        
        var uid : String
        uid = UserDefaults.standard.string(forKey: "uid")!
        var isConnected : String
        isConnected = UserDefaults.standard.string(forKey: "isConnected")!

        if  isConnected == "true" && uid != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"db" : self.dbCourse]
            let urlString = "\(global.domaine)/claroline/api_mobile/description.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                if let json = response.result.value  as? NSArray {
                    for element in json{
                        if let el = element as? NSDictionary {
                            if let visibilty = el.value(forKey: "visibility") {
                                
                                if visibilty as? String == "SHOW" {
                                    if let title = el.value(forKey: "title") {
                                        self.titleArray.append(title as! String)
                                    }
                                    if let content = el.value(forKey: "content") {
                                        self.contentArray.append(content as! String)
                                    }

                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    OperationQueue.main.addOperation({
                        self.viewTable.reloadData()
                    })
                }
                self.items = self.titleArray.count + 1
            }
            
        }
                
    }
    
    // MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header", for: indexPath) as! HeaderCellDescription
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            cell.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
            return cell
        case 1...self.items:
            let cell = tableView.dequeueReusableCell(withIdentifier: "contentCell", for: indexPath) as! ContentCell
            cell.title.text = self.titleArray[indexPath.row - 1]
            let htmlCode = "<html><head><title></title><meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'></head> <body style='hieght:245px;margin=5px;'>"+self.contentArray[indexPath.row-1]+"</body></html>"
            cell.webView.loadHTMLString(htmlCode, baseURL: nil)
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! MenuCellCourse
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.row == 0{
            return CGFloat(120)
        }
        
        return CGFloat(300) /// 220 is my static value
    }
    
    

}

class HeaderCellDescription : UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profesor: UILabel!
    @IBOutlet weak var codeCourse: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.profilePic.layer.cornerRadius = 25
        //        self.profilePic.clipsToBounds = true
    }
}

class ContentCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var webView: WKWebView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        

        
        //        self.profilePic.layer.cornerRadius = 25
        //        self.profilePic.clipsToBounds = true
    }
}
