
import UIKit
import Alamofire
import MBProgressHUD
import WebKit
import SwiftyJSON
import SDLoader
import HTMLString

class Document_links: UIViewController, UITableViewDelegate, UITableViewDataSource,cellDelegate  {
    let global = Global()
    
    let sdLoader = SDLoader()
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var viewTableDoc: UITableView!
    let menuTitles = ["Cours Description","Assignements", "Chat"]
    //course  attribute
    var codeCoursText : String = UserDefaults.standard.string(forKey: "codeCoursText")!
    var profesorText : String = UserDefaults.standard.string(forKey: "profesorText")!
    var titleText: String = UserDefaults.standard.string(forKey: "titleText")!
    var dbCourse : String = UserDefaults.standard.string(forKey: "dbCourse")!
    var categoryCode : String = UserDefaults.standard.string(forKey: "categoryCode")!
    var directory : String = UserDefaults.standard.string(forKey: "directory")!
    var isCourseManager : String = UserDefaults.standard.string(forKey: "isCourseManager")!
    var language : String = UserDefaults.standard.string(forKey: "language")!
    var sysCode : String = UserDefaults.standard.string(forKey: "sysCode")!
    //file attribute
    var nameFilesArray = [String]()
    var visibilityArray = [String]()
    var imgArray = [String]()
    var extArray = [String]()
    var typeArray = [String]()
    var lastContentOffset: CGFloat = 0.0
    var urlsArray = [String]()
    var urlpass = ""
    let header_i = 2
    // collection view
    var urlpass_array = [String]()
    var urlpass_array_name = [String]()
    
    
    
    var items = 0
    // view
    var fileLocalURL = [Int:URL]()
    var header_name = "Document & Links"
    func customization() {
        //self.viewTable.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTableDoc.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.viewTableDoc.rowHeight = UITableViewAutomaticDimension
        self.viewTableDoc.estimatedRowHeight = 340
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        self.MyDocument(url: "")
        self.customization()
        sdLoader.startAnimating(atView: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function
    func MyDocument(url : String){
        sdLoader.startAnimating(atView: self.view)
        var uid : String
        uid = UserDefaults.standard.string(forKey: "uid")!
        var isConnected : String
        isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        if  isConnected == "true" && uid != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"cid" : sysCode,"dbtable" : dbCourse,"urlpass" : url]
            let urlString = "\(global.domaine)/claroline/api_mobile/document_api.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                    print(response)
                if let json = response.result.value  as? NSArray {
                    for doc in json{
                        
                        if let documents = doc as? NSDictionary {
                            if  let visibility = documents["visibility"], let ext = documents["ext"], let name = documents["name"]{
                                if ext as! String != "php" , ext as!String != "url" , ext as! String != "bak" , visibility as! String != "i" , name as! String != "" {
                                    //path
                                    if let path = documents["path"]{
                                        self.urlsArray.append((path as! String).removingHTMLEntities)
                                    }
                                    
                                    self.nameFilesArray.append((name as! String).removingHTMLEntities)
                                    self.extArray.append((ext as! String).removingHTMLEntities)
                                    if  let type = documents["type"]{
                                        if type as! String == "1" {
                                            self.typeArray.append("1")
                                            self.imgArray.append("folder")
                                        }else{
                                            self.typeArray.append("2")
                                            if ext  as! String == "doc" ||  ext  as! String == "docx"  {
                                                self.imgArray.append("doc")
                                            }else if ext  as! String == "xls" || ext  as! String == "xlsx" {
                                                self.imgArray.append("excel")
                                            }else if ext  as! String == "ppt" || ext  as! String == "pptx" {
                                                self.imgArray.append("powerpoint")
                                            }else if ext  as! String == "flv" || ext  as! String == "mp4" {
                                                self.imgArray.append("video")
                                            }else{
                                                self.imgArray.append("file")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.viewTableDoc.reloadData()
                    })
                }
                self.items = self.nameFilesArray.count + 2
                self.sdLoader.stopAnimation()
            }
            
        }
        
    }
    
    func reload(url: String){
        self.MyDocument(url: url)
        
    }
    // MARK: - Table view data source
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerdoc", for: indexPath) as! HeaderDocumentCell
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            cell.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
            return cell
        case 1:
            if self.urlpass_array == [String](){
                self.urlpass_array.append("")
                self.urlpass_array_name.append("/Document")
                
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "tabCell", for: indexPath) as! TabCell
            cell.name_array = self.urlpass_array_name
            cell.url_array  = self.urlpass_array
            cell.reload()
            return cell
        case 2...:
            let cell = tableView.dequeueReusableCell(withIdentifier: "menudocument", for: indexPath) as! MenuCellDocument
            cell.menuTitles.text = self.nameFilesArray[indexPath.row - header_i]
            cell.menuIcon.image = UIImage.init(named: self.imgArray[indexPath.row - header_i])
            cell.delegate = self
            if self.typeArray[indexPath.row - header_i] == "1" {
               // cell.viewBtn.isHidden = true
               // cell.downloadBtn.isHidden = true
                var FOLDER_NAME = ""
                FOLDER_NAME = self.sysCode
                let fileManager = FileManager.default
                // creation du dossier s'il n"existe
                if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let filePath =  tDocumentDirectory.appendingPathComponent("\(FOLDER_NAME)/\(self.nameFilesArray[indexPath.row - header_i])")
                    if !fileManager.fileExists(atPath: filePath.path) {
                        do {
                            try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                            NSLog("Document directory is created")
                        } catch {
                            NSLog("Couldn't create document directory")
                        }
                    }
                    NSLog("Document directory is \(filePath)")
                }
            }else{
                
                if self.extArray[indexPath.row - header_i] != "php" &&  self.extArray[indexPath.row - header_i] != "url" && self.extArray[indexPath.row - header_i] != "bak" {
                    let fileManager = FileManager.default
                    if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let filePath = tDocumentDirectory.appendingPathComponent("\(self.sysCode)\(self.urlpass)/\(self.nameFilesArray[indexPath.row - header_i]).\(self.extArray[indexPath.row - header_i])")
                        if !fileManager.fileExists(atPath: filePath.path) {
                           // cell.viewBtn.isEnabled = false
                            //cell.downloadBtn.isEnabled = true
                        }else{
                            //cell.viewBtn.isEnabled = true
                            //cell.downloadBtn.isEnabled = false
                            self.fileLocalURL[indexPath.row ] =  filePath
                        }
                    }
                }
            }
            
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerdoc", for: indexPath) as! HeaderDocumentCell
            cell.name.text = self.titleText
            cell.codeCourse.text = self.codeCoursText
            cell.profesor.text = self.profesorText
            cell.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 && indexPath.row != 1 {
            if  self.typeArray[indexPath.row - header_i] == "1" {
                urlpass = ""
                for i in 1...self.urlpass_array.count {
                    urlpass += urlpass_array[i - 1]
                }
                self.urlpass += "/"+self.nameFilesArray[indexPath.row - header_i]
                self.urlpass_array.append(urlpass)
                self.urlpass_array_name.append("> "+self.nameFilesArray[indexPath.row - header_i])
                
                nameFilesArray = [String]()
                visibilityArray = [String]()
                imgArray = [String]()
                extArray = [String]()
                typeArray = [String]()
                urlsArray = [String]()
                
                self.MyDocument(url: urlpass)
            }else{
                print(self.extArray[indexPath.row - header_i])
                print(self.extArray[indexPath.row - header_i])
                print(self.extArray[indexPath.row - header_i])
                if self.extArray[indexPath.row - header_i] != "php" &&  self.extArray[indexPath.row - header_i] != "url" && self.extArray[indexPath.row - header_i] != "bak" {
                    let fileManager = FileManager.default
                    if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let filePath = tDocumentDirectory.appendingPathComponent("\(self.sysCode)\(self.urlpass)/\(self.nameFilesArray[indexPath.row - header_i]).\(self.extArray[indexPath.row - header_i])")
                        print(filePath)
                        if !fileManager.fileExists(atPath: filePath.path) {
                            self.downloadFileWithIndex(ind: indexPath.row)
                        }else{
                            //self.downloadFileWithIndex(ind: indexPath.row)
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReaderViewController") as! ReaderViewController
                            let url:URL = fileLocalURL[indexPath.row]!
                            vc.url = url
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 120
        case 1...:
            return 50
        default:
            return 120
        }
    }
    // download btn && view btn
    
    func didClickDownloadButton(menudocument: UITableViewCell) {
        let indexPath = self.viewTableDoc.indexPath(for: menudocument)
        if let index = indexPath?.row {
           // (menudocument as! MenuCellDocument).viewBtn.isEnabled = true
           // (menudocument as! MenuCellDocument).downloadBtn.isEnabled = false
            downloadFileWithIndex(ind: index)
        }
        
    }
    
    @IBAction func ClickBack(_ sender: Any) {
        print(self.urlpass_array)
        
        print(self.urlpass_array_name)
        print(self.urlpass_array.count)
        if(self.urlpass_array.count > 1){
            self.urlpass_array_name.remove(at: self.urlpass_array_name.count - 1)
            self.urlpass_array.remove(at: self.urlpass_array.count - 1)
            
            var urlpass = ""
            
            for i in 1...self.urlpass_array.count {
                urlpass += urlpass_array[i - 1]
            }
            nameFilesArray = [String]()
            visibilityArray = [String]()
            imgArray = [String]()
            extArray = [String]()
            typeArray = [String]()
            urlsArray = [String]()
            
            self.MyDocument(url: urlpass)
        }
        
    }
    func didClickViewButton(menudocument: UITableViewCell) {
        let indexPath = self.viewTableDoc.indexPath(for: menudocument)
        if let index = indexPath?.row {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReaderViewController") as! ReaderViewController
            let url:URL = fileLocalURL[index]!
            vc.url = url
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    // download file
    
    func downloadFileWithIndex(ind:Int) {
        
        //--2.--//
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.annularDeterminate
        hud.label.text = "Loading..."
        //--2.--//
        
        //--1.--//
        
        let urlString = urlsArray[ind - 2]
        
        var FOLDER_NAME = ""
        FOLDER_NAME = self.sysCode
        //
        var fileURL:URL! = nil
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            fileURL = documentsURL.appendingPathComponent("\(FOLDER_NAME)/\(self.nameFilesArray[ind - 2]).\(self.extArray[ind - 2])")
            if self.urlpass != "" {
                 fileURL = documentsURL.appendingPathComponent("\(FOLDER_NAME)/\(self.urlpass)/\(self.nameFilesArray[ind - 2]).\(self.extArray[ind - 2])")
            }
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlString, to: destination).downloadProgress(closure: { (prog) in
            hud.progress = Float(prog.fractionCompleted)
        }).response { response in
            hud.hide(animated: true)
            if response.error == nil, let filePath = response.destinationURL?.path {
                self.fileLocalURL[ind] = NSURL(string: filePath)! as URL
                print(ind)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReaderViewController") as! ReaderViewController
                let url:URL = self.fileLocalURL[ind]!
                vc.url = url
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        //--1.--//
    }
    
}

// Header
class HeaderDocumentCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profesor: UILabel!
    @IBOutlet weak var codeCourse: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.profilePic.layer.cornerRadius = 25
        //        self.profilePic.clipsToBounds = true
    }
}






