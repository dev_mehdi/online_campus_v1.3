

import UIKit
import Foundation
//TableView Custom Classes
class TabCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var url_array = [String]()
    var name_array = [String]()
    //var size:CGFloat = 80
    //var tabs_o = [Tab]()
    func customization() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
//        Tab.fetchData { [weak self] tabs_o in
//            guard let weakSelf = self else {
//                return
//            }
//            weakSelf.tabs_o = tabs_o
//            weakSelf.collectionView.reloadData()
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.url_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tapCvCell", for: indexPath) as! tabCvCell
        cell.tab.text = self.name_array[indexPath.row]
        cell.url = self.url_array[indexPath.row]
        //size =  cell.tab.intrinsicContentSize.width + 10
        //print(size)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let name: String = self.name_array[indexPath.row]
        let size: CGSize = name.size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0)])
        //print(size.width)
        return CGSize.init(width: size.width + 10, height: 27)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.customization()
    }
    
    func reload(){
        
        self.collectionView.reloadData()
        
    }
    
}

class tabCvCell: UICollectionViewCell {
//    @IBOutlet weak var channelPic: UIImageView!
    
    @IBOutlet weak var tab: UILabel!
    var url: String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.channelPic.layer.cornerRadius = 25
//        self.channelPic.clipsToBounds = true
    }
    
    
}

// tab Class

class Tab {
    
    let name: String
    let url: String
    //let image: UIImage
    var subscribers = 0
    
    class func fetchData(completion: @escaping (([Tab]) -> Void)) {
        var items = [Tab]()
        items.append(Tab.init(name: "/Document",url: ""))
        
        
        completion(items)
    }
    init(name: String,url:String) {
        self.name = name
        self.url = url
    }
}

