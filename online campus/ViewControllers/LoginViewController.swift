

import Foundation
import UIKit
import Alamofire



class LoginViewController: UIViewController,UITextFieldDelegate,UserLogout{
    let global = Global()
    func userlogoutsetting() {
        
    }
    
    @IBOutlet weak var loginfield: LoginTextField!
    
    @IBOutlet weak var passwordField: LoginTextField!
    @IBOutlet weak var passwordButton: UIButton!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString = NSAttributedString(string: "Forgot your Password?", attributes: [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.underlineStyle:1])
        
        passwordButton.setAttributedTitle(attributedString, for: .normal)
       
//        let toolbar = UIToolbar()
//        toolbar.sizeToFit()
//        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(doneClicked))
//        toolbar.setItems([flexibleSpace,doneButton], animated: false)
//
//        loginfield.inputAccessoryView = toolbar
//        passwordField.inputAccessoryView = toolbar
       
    }
    //show hide keyboard
    
    // done button
    @objc func doneClicked(){
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Login
    @IBAction func login(_ sender: Any) {
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(UserDefaults.standard.string(forKey: "isConnected"))
        
        
        
        let logintext = loginfield.text
        let passwordtext = passwordField.text
        if  logintext == "" && passwordtext == "" {
            createAlert(title: "Warning!", message: "Please enter your Login and Password")
        }else if logintext == "" {
            createAlert(title: "Warning!", message: "Please enter your Login")
        }else if passwordtext == "" {
            createAlert(title: "Warning!", message: "Please enter your Password")
        }else {
            if  logintext != "" && passwordtext != "" {
                let parameters = ["login" : logintext!,"password" : passwordtext!]
                let urlString = "\(global.domaine)/claroline/api_mobile/login.php"
                
                Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                    
                    if let json = response.result.value  as? NSDictionary {
                        var login : String
                        var password : String
                        var isConnected : String
                        var uid : String
                        
                        if  json["connected"] as! String == "true" {
                            //Create a local user with setting
                            login = json["login"] as! String
                            password = json["password"] as! String
                            isConnected = json["connected"] as! String
                            var nom : String = "nom"
                            var prenom : String = "prenom"
                            var email : String = "email"
                            var officialCode : String = "officialCode"
                            var phoneNumber : String = "phoneNumber"
                            var picture : String = "/photo/defaultPd.png"
                            
                            uid = json["uid"] as! String
                            nom = json["nom"] as! String
                            prenom = json["prenom"] as! String
                            email = json["email"] as! String
                            officialCode = json["officialCode"] as! String
                            phoneNumber = json["phoneNumber"] as! String
                            picture = json["picture"] as! String
                            let user = UserDefaults.standard
                            user.set(password, forKey: "password")
                            user.set(login , forKey: "login")
                            user.set(isConnected, forKey: "isConnected")
                            user.set(uid, forKey: "uid")
                            user.set(nom, forKey: "nom")
                            user.set(prenom, forKey: "prenom")
                            user.set(email, forKey: "email")
                            user.set(officialCode, forKey: "officialCode")
                            user.set(phoneNumber, forKey: "phoneNumber")
                            user.set(picture, forKey: "picture")
                            if picture ==  "" {
                                let defaultPic : String = "/photo/defaultPd.png"
                                user.set(defaultPic, forKey: "picture")
                            }
                            
                            user.synchronize()
                            print(UserDefaults.standard.string(forKey: "officialCode"))
                            let nc = NotificationCenter.default
                            nc.post(name: .reloadDataAfterLogin, object: nil)
                           
                            self.dismiss(animated: true, completion: nil)
                            
                            //ViewController.myCourses()
                        }else {
                            let alert = UIAlertController(title: "Warning!", message: "Your Login or Password is incorrect", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                }
            }else {
                createAlert(title: "Warning!", message: "Your Login or Password is incorrect")
            }
            
        }
        
        
    }
   

    // creation alert
    func createAlert(title : String , message : String ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        //creating on Button
        alert.addAction(UIAlertAction(title : "OK",style : UIAlertActionStyle.default,handler :{(action) in alert.dismiss(animated: true, completion: nil)}))
        
        self.present(alert,animated: true,completion: nil)
     }
    
    // oriantation
//    override var shouldAutorotate: Bool{
//        return false;
//    }
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
//        return UIInterfaceOrientationMask.portrait;
//    }
    
//    //keyboard
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool
//    {
//        textField.resignFirstResponder()
//        return true
//    }
    
    

    
    
}
