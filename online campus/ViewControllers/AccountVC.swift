

import UIKit

class AccountVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let global = Global()
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    
    var menuTitles = [String]()
    
    var items = 0
    var user = User.init(name: "Loading", profilePic: UIImage(), playlists: [Playlist]())
    var lastContentOffset: CGFloat = 0.0
    
    //MARK: Methods

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.customization()
    }
    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.set("false", forKey: "login")
        UserDefaults.standard.set("false", forKey: "password")
        UserDefaults.standard.set("false", forKey: "isConnected")
        UserDefaults.standard.synchronize()
        let loginView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login")
        
        self.present(loginView, animated: false, completion: nil)
    }
    func customization() {
        if UserDefaults.standard.string(forKey: "email") != nil && UserDefaults.standard.string(forKey: "email") != ""{
            menuTitles.append(UserDefaults.standard.string(forKey: "email")!)
        }
        if UserDefaults.standard.string(forKey: "phoneNumber") != nil && UserDefaults.standard.string(forKey: "phoneNumber") != ""{
            menuTitles.append(UserDefaults.standard.string(forKey: "phoneNumber")!)
        }
        if UserDefaults.standard.string(forKey: "officialCode") != nil && UserDefaults.standard.string(forKey: "officialCode") != ""{
            menuTitles.append(UserDefaults.standard.string(forKey: "officialCode")!)
        }
        items = menuTitles.count + 1
        print(items)
        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300
//        User.fetchData { [weak self] response in
//            guard let weakSelf = self else {
//                return
//            }
//            weakSelf.user = response
//            weakSelf.items += response.playlists.count
//            weakSelf.tableView.reloadData()
//        }
        var image = UIImage()
        var url : URL = URL(string: "\(global.domaine)/claroline/admin")!
        if  UserDefaults.standard.string(forKey: "picture") != nil {
            url = URL(string: "\(global.domaine)/claroline/admin\(UserDefaults.standard.string(forKey: "picture")!)")!
            let data = try? Data(contentsOf: url)
            
            if let imageData = data {
                image = UIImage(data: imageData)!
            }
            user = User.init(name: "\(UserDefaults.standard.string(forKey: "prenom")!) \(UserDefaults.standard.string(forKey: "nom")!)", profilePic: image)

        }
        //notification
        let nc = NotificationCenter.default
        nc.post(name: .logOut, object: nil)
        nc.addObserver(self, selector: #selector(logout), name: .logOut, object: nil)
        
        
        self.tableView.reloadData()
    }
    
    @objc func logout(){
        UserDefaults.standard.set("false", forKey: "login")
        UserDefaults.standard.set("false", forKey: "password")
        UserDefaults.standard.set("false", forKey: "isConnected")
        UserDefaults.standard.synchronize()
        let loginView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login")
        
        self.present(loginView, animated: false, completion: nil)
    }
    
    // MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header", for: indexPath) as! AccountHeaderCell
            cell.name.text = self.user.name
            cell.profilePic.image = self.user.profilePic
            return cell
        case 1...self.items:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! AccountMenuCell
            cell.menuTitles.text = self.menuTitles[indexPath.row - 1]
            //cell.menuIcon.image = UIImage.init(named: self.menuTitles[indexPath.row - 1])
           return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Menu", for: indexPath) as! AccountMenuCell
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: false)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: true)
        }
    }
    
    //MARK: -  ViewController Lifecylce
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.customization()
    }
    
}

class AccountHeaderCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePic.layer.cornerRadius = 25
        self.profilePic.clipsToBounds = true
    }
}

class AccountMenuCell: UITableViewCell {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitles: UILabel!
    
}

class AccountPlaylistCell: UITableViewCell {
    
    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var numberOfVideos: UILabel!
    
    override func awakeFromNib() {
        self.pic.layer.cornerRadius = 5
        self.pic.clipsToBounds = true
    }
}



