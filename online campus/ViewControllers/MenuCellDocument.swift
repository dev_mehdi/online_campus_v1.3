

import UIKit

// Cell Menu Document list
protocol cellDelegate {
    func didClickDownloadButton(menudocument:UITableViewCell)
    func didClickViewButton(menudocument:UITableViewCell)
}

class MenuCellDocument: UITableViewCell {
    
    var delegate:cellDelegate?
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitles: UILabel!
    //@IBOutlet weak var viewBtn: UIButton!
    //@IBOutlet weak var downloadBtn: UIButton!
    
    @IBAction func downloadTapped(_ sender: Any) {
        print("downloadTapped")
        delegate?.didClickDownloadButton(menudocument: self)
    }
    
    @IBAction func viewTapped(_ sender: Any) {
        print("viewTapped")
        delegate?.didClickViewButton(menudocument: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // viewBtn.isEnabled = false
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

