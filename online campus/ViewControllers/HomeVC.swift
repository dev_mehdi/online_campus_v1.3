
import UIKit
import Alamofire
import SwiftyJSON
import SDLoader

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{
   
    
    
    let global = Global()
    
    
    
    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewSession: UIView!
    
    
    let sdLoader = SDLoader()

    let logo = "iul logo"
    
    let hello_text = "Welcome to IUL eCampus, our records indicate that you are taking classes this semester.Enjoy your learning experience, we are behind you 100% .Thank you."
    
    let titleSession = "Online Calendar - Academic Year 18/19"
    var session_bba = ["BBA"]
    var startDate_bba = [""]
    var endDate_bba = [""]
    var session_mba = ["MBA"]
    var startDate_mba = [""]
    var endDate_mba = [""]
    //var session_dba = ["DBA","Session 1","Session 2","Session 3","Session 4","Session 5"]
    var session_dba = ["DBA"]
    var startDate_dba = [""]
    var endDate_dba = [""]
    var niveau = ["bba","mba","dba"]
    var session = [String]()
    var startDate = [String]()
    var endDate = [String]()
    var indexNiveau = 0
    var verify = 0
    
     var items = 0
    
    var lastContentOffset: CGFloat = 0.0
    // delegate methods
    
    //MARK: Methods
    func customization() {
        //self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 30, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 30, 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 300
        tableView.dataSource = self
        //TODO: Register your ForumTableViewCell.xib file here:
        tableView.register(UINib(nibName:"SessionViewCell",bundle: nil), forCellReuseIdentifier: "sessionCell")
        //TODO: Register your ForumTableViewCell.xib file here:
        tableView.register(UINib(nibName:"HeaderSessionViewCell",bundle: nil), forCellReuseIdentifier: "headerSessionCell")
        viewSession.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
        items = 0
        self.session_bba = ["BBA"]
        self.startDate_bba = [""]
        self.endDate_bba = [""]
        self.session_mba = ["MBA"]
        self.startDate_mba = [""]
        self.endDate_mba = [""]
        self.session_dba = ["DBA"]
        self.indexNiveau = 0
        self.session = []
        self.startDate = []
        self.endDate = []
        
        
        
    }
    //MARK: -  ViewController Lifecylce
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customization()
        let isuserlogin = UserDefaults.standard.bool(forKey: "isConnected")
        if  (!isuserlogin) {
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
        
        sdLoader.startAnimating(atView: self.view)
        self.Calendar() { response in
            // Do your stuff here
            print("Data -------")
            print(response)
            
            for actor in response{
                if let calendar = actor as? NSDictionary {
                    if let program = calendar["program"]  {
                        if program as! String == "BBA" {
                            self.session_bba.append("\(calendar["session"] as! String) \(calendar["year"] as! String)")
                            self.startDate_bba.append("\(calendar["start"] as! String)")
                            self.endDate_bba.append("\(calendar["end"] as! String)")
                        }
                        if program as! String == "MBA" {
                            self.session_mba.append("\(calendar["session"] as! String) \(calendar["year"] as! String)")
                            self.startDate_mba.append("\(calendar["start"] as! String)")
                            self.endDate_mba.append("\(calendar["end"] as! String)")
                        }
                        if program as! String == "DBA" {
                            self.session_dba.append("\(calendar["session"] as! String) \(calendar["year"] as! String)")
                            self.startDate_dba.append("\(calendar["start"] as! String)")
                            self.endDate_dba.append("\(calendar["end"] as! String)")
                        }
                    }
                }
            }
            /// validation des donnée            
            if  UserDefaults.standard.string(forKey: "officialCode") != nil {
                let officialCode = UserDefaults.standard.string(forKey: "officialCode")
                if officialCode == "Admin" || officialCode == "Agent"  || officialCode == "Faculty" || officialCode == "faculty" || officialCode == "guest" || officialCode == "BBA" || officialCode == "bba" {
                    print("BBA SESSION COUNT")
                    self.indexNiveau = 0
                    self.items = self.items + self.session_bba.count
                    self.session = self.session + self.session_bba
                    self.startDate = self.startDate + self.startDate_bba
                    self.endDate = self.endDate + self.endDate_bba
                }
                if officialCode == "Admin" || officialCode == "Agent"  || officialCode == "Faculty" || officialCode == "faculty" || officialCode == "guest" || officialCode == "mba" || officialCode == "MBA" {
                    print("MBA close")
                    self.indexNiveau = 1
                    self.items = self.items + self.session_mba.count
                    self.session = self.session + self.session_mba
                    self.startDate = self.startDate + self.startDate_mba
                    self.endDate = self.endDate + self.endDate_mba
                }
                if officialCode == "Admin" || officialCode == "Agent"  || officialCode == "Faculty" || officialCode == "faculty" || officialCode == "guest" || officialCode == "dba" || officialCode == "DBA" {
                    self.indexNiveau = 2
                    self.items = self.items + self.session_dba.count
                    self.session = self.session + self.session_dba
                    self.startDate = self.startDate + self.startDate_dba
                    self.endDate = self.endDate + self.endDate_dba
                }
            }
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
            self.sdLoader.stopAnimation()
        }
        
        let nc = NotificationCenter.default
        nc.post(name: .reloadDataAfterLogin, object: nil)
        nc.addObserver(self, selector: #selector(tablereload), name: .reloadDataAfterLogin, object: nil)
        
        
    }
    
    override func viewDidLoad() {
        
    }
    
    
    
    
    @objc func tablereload() {
        print("LOGIN")
        self.tableView.reloadData()
    }
    
    
    func Calendar(completion: @escaping (NSArray) -> Void){
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        if  isConnected == "true" && uid != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid]
            let urlString = "\(global.domaine)/claroline/api_mobile/calendar.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                switch response.result {
                    case .success:
                        print("Validation Successful")
                        completion(response.result.value as! NSArray)
                    case .failure(let error):
                        print(error)
                }
            }
        }
    }
    
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "sessionCell", for: indexPath) as! SessionViewCell
            print(UserDefaults.standard.string(forKey: "officialCode"))
            if  UserDefaults.standard.string(forKey: "officialCode") != nil {
                    if self.session[indexPath.row] == "BBA" || self.session[indexPath.row] == "MBA" || self.session[indexPath.row] == "DBA" {
                         let cell = tableView.dequeueReusableCell(withIdentifier: "headerSessionCell", for: indexPath) as! HeaderSessionViewCell
                        cell.session.text = self.session[indexPath.row]
                        cell.viewSession.backgroundColor = UIColor(red:0.20, green:0.30, blue:0.36, alpha:1.0)
                        return cell
                    }else{
                        cell.viewSession.backgroundColor = UIColor(red:0.20, green:0.30, blue:0.36, alpha:1.0)
                        cell.ViewDate.backgroundColor = UIColor(red:0.08, green:0.70, blue:0.47, alpha:1.0)
                        cell.StartView.backgroundColor = UIColor(red:0.08, green:0.70, blue:0.47, alpha:1.0)
                        cell.session.text = self.session[indexPath.row]
                        cell.startDate.text = "Start Date : "+self.startDate[indexPath.row]
                        cell.endDate.text = "End Date : "+self.endDate[indexPath.row]
                        return cell
                    }
            }
        
            return cell
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if self.session[indexPath.row] == "BBA" || self.session[indexPath.row] == "MBA" || self.session[indexPath.row] == "DBA" {
//            return 30
//        }else{
//            return 111
//        }
//    }

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //NotificationCenter.default.post(name: NSNotification.Name("open"), object: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: false)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("hide"), object: true)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.lastContentOffset = scrollView.contentOffset.y;
    }
    
}

