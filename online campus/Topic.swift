//
//  Topic.swift
//  online campus
//
//  Created by mehdi on 10/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation
import UIKit

class Topic {
    
    //MARK: Properties
    var id: String = "0"
    var name: String = "Topic Name"
    var poster: String = "poster name"
    var post_number: String = "0"
    var seen_number: String = "0"
    var last_message: String = "Last Message Date"
    
    //MARK: Inits
    
    init(id: String,name: String,poster: String,post_number: String,seen_number:String,last_message: String) {
        self.id = id
        self.name = name
        self.poster = poster
        self.post_number = post_number
        self.seen_number = seen_number
        self.last_message = last_message
    }
    init(){
    }
}
