//
//  HeaderSessionViewCell.swift
//  online campus
//
//  Created by mehdi on 24/08/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class HeaderSessionViewCell: UITableViewCell {

    
    @IBOutlet weak var viewSession: UIView!
    @IBOutlet weak var session: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
