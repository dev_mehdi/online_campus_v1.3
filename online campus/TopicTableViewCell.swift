//
//  TopicTableViewCell.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    //design
    
    @IBOutlet weak var containerViewDetail: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var containerView: UIView!
    //properties
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var poster: UILabel!
    
    @IBOutlet weak var seen_number: UILabel!
    
    @IBOutlet weak var post_number: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
