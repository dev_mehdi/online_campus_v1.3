//
//  ForumTableViewCell.swift
//  online campus
//
//  Created by mehdi on 05/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class ForumTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var barView: UIView!
    
    @IBOutlet weak var ForumDescription: UILabel!
    
    @IBOutlet weak var topicNumber: UILabel!
    
    @IBOutlet weak var postsNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
