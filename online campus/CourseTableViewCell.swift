//
//  CourseTableViewCell.swift
//  online campus
//
//  Created by mehdi on 24/08/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {

    @IBOutlet weak var adminname: UILabel!
    @IBOutlet weak var codeCourses: UILabel!
    @IBOutlet weak var titleCourse: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var courseViewTitle: UIView!
    @IBOutlet weak var ViewImage: UIView!
    @IBOutlet weak var viewCourse: UIView!
    @IBOutlet weak var ViewProf: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
