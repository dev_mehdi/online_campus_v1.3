//
//  Post.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation
import UIKit

class Post {
    
    //MARK: Properties
    var id: String = "0"
    var poster: String = "Poster Name"
    var posted: String = "post date"
    var post: String = "message"
    
    //MARK: Inits
    
    init(id: String,poster: String,posted: String,post: String) {
        self.id = id
        self.poster = poster
        self.posted = posted
        self.post = post
    }
    init(){
    }
}
