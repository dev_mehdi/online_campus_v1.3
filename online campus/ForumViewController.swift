//
//  ForumViewController.swift
//  online campus
//
//  Created by mehdi on 05/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ForumViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let global = Global()
    var codeCoursText : String = UserDefaults.standard.string(forKey: "codeCoursText")!
    var profesorText : String = UserDefaults.standard.string(forKey: "profesorText")!
    var titleText: String = UserDefaults.standard.string(forKey: "titleText")!
    var dbCourse : String = UserDefaults.standard.string(forKey: "dbCourse")!
    var categoryCode : String = UserDefaults.standard.string(forKey: "categoryCode")!
    var directory : String = UserDefaults.standard.string(forKey: "directory")!
    var isCourseManager : String = UserDefaults.standard.string(forKey: "isCourseManager")!
    var language : String = UserDefaults.standard.string(forKey: "language")!
    var sysCode : String = UserDefaults.standard.string(forKey: "sysCode")!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var profesorLabel: UILabel!
    
    @IBOutlet weak var codeCoursLabel: UILabel!
    
    @IBOutlet weak var tableViewForum: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    
    var forums  = [Forum]()
    var items = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
       
        
        //TODO: Set Yourself as Delegate and datasource here:
        tableViewForum.delegate = self
        tableViewForum.dataSource = self
        
        //TODO: Register your ForumTableViewCell.xib file here:
        tableViewForum.register(UINib(nibName:"ForumTableViewCell",bundle: nil), forCellReuseIdentifier: "forumCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureTableView()
        SVProgressHUD.show()
        forumsList()
    }
    
    
    func configureTableView(){
        tableViewForum.rowHeight = UITableViewAutomaticDimension
        tableViewForum.estimatedRowHeight = 300
        tableViewForum.separatorStyle = .none
        tableViewForum.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        headerView.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
        self.titleLabel.text = titleText
        self.codeCoursLabel.text = codeCoursText
        self.profesorLabel.text = profesorText
    }
    
    //MARK: - Fetch List of Forums
    func forumsList(){
         ///SVProgressHUD.show()
        forums = []
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        
        var dbCourse:String = ""
        if  UserDefaults.standard.string(forKey: "dbCourse") != nil {
            dbCourse = UserDefaults.standard.string(forKey: "dbCourse")!
        }
        if  isConnected == "true" && uid != "" && dbCourse != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"dbCourse": dbCourse]
            let urlString = "\(global.domaine)/claroline/api_mobile/list_forum.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                if let json = response.result.value  as? NSArray {
                    for forums in json{
                        if let forum = forums as? NSDictionary {
                            let fr:Forum = Forum()
                            if let id = forum["id"] as? String {
                                fr.id = id
                            }
                            if let name = forum["name"] as? String {
                                fr.name = name
                            }
                            if let description = forum["description"] as? String {
                                fr.description = description
                            }
                            if let total_topics = forum["total_topics"] as? String {
                                fr.total_topics = total_topics
                            }
                            if let total_posts = forum["total_posts"] as? String {
                                fr.total_posts = total_posts
                            }
                            if let last_message = forum["last_message"] as? String {
                                fr.last_message = last_message
                            }
                            
                            self.forums.append(fr)
                            
                            
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.tableViewForum.reloadData()
                    })
                    SVProgressHUD.dismiss()
                }
                self.items = self.forums.count
            }
            
        }
        
    }
    //MARK: - TAble View Datasource
    
    //TODO: Declare numberOfRowsInSection here:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    
    //TODO: Declare cellForRowAtIndexPath here:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forumCell", for: indexPath) as! ForumTableViewCell
        cell.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        cell.barView.backgroundColor = UIColor(red:0.93, green:0.64, blue:0.08, alpha:1.0)
        cell.name.text = self.forums[indexPath.row].name
        cell.ForumDescription.text = self.forums[indexPath.row].description
        cell.topicNumber.text = self.forums[indexPath.row].total_topics
        cell.postsNumber.text = self.forums[indexPath.row].total_posts
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "topic") as! TopicViewController
        vc.forum_id = self.forums[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    

}
