//
//  TopicViewController.swift
//  online campus
//
//  Created by mehdi on 06/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


class TopicViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let global = Global()
    var codeCoursText : String = UserDefaults.standard.string(forKey: "codeCoursText")!
    var profesorText : String = UserDefaults.standard.string(forKey: "profesorText")!
    var titleText: String = UserDefaults.standard.string(forKey: "titleText")!
    var dbCourse : String = UserDefaults.standard.string(forKey: "dbCourse")!
    var categoryCode : String = UserDefaults.standard.string(forKey: "categoryCode")!
    var directory : String = UserDefaults.standard.string(forKey: "directory")!
    var isCourseManager : String = UserDefaults.standard.string(forKey: "isCourseManager")!
    var language : String = UserDefaults.standard.string(forKey: "language")!
    var sysCode : String = UserDefaults.standard.string(forKey: "sysCode")!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var profesorLabel: UILabel!
    
    @IBOutlet weak var codeCoursLabel: UILabel!
    
    @IBOutlet weak var topicTableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    
    var forum_id:String = ""
    var topics  = [Topic]()
    var items = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backItem?.title = ""
        
        //TODO: Set Yourself as Delegate and datasource here:
        topicTableView.delegate = self
        topicTableView.dataSource = self
        headerView.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)

        //TODO: Register your ForumTableViewCell.xib file here:
        topicTableView.register(UINib(nibName:"TopicTableViewCell",bundle: nil), forCellReuseIdentifier: "topicCell")
        
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backItem?.title = ""
        SVProgressHUD.show()
        configureTableView()
        topicsList()
    }
    
    //MARK: - Configure Table View
    func configureTableView(){
        topicTableView.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        self.titleLabel.text = titleText
        self.codeCoursLabel.text = codeCoursText
        self.profesorLabel.text = profesorText
    }
    
    //MARK: - Fetch List of Forums
    func topicsList(){
        topics = []
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        
        var dbCourse:String = ""
        if  UserDefaults.standard.string(forKey: "dbCourse") != nil {
            dbCourse = UserDefaults.standard.string(forKey: "dbCourse")!
        }
        if  isConnected == "true" && uid != "" && dbCourse != "" && self.forum_id != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"dbCourse" : dbCourse,"forum_id" : self.forum_id]
            let urlString = "\(global.domaine)/claroline/api_mobile/list_topic.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                //print(response)
                if let json = response.result.value  as? NSArray {
                    for topics in json{
                        if let topic = topics as? NSDictionary {
                            let tp:Topic = Topic()
                            if let id = topic["id"] as? String {
                                tp.id = id
                            }
                            if let name = topic["name"] as? String {
                                tp.name = name
                            }
                            if let post_number = topic["post_number"] as? String {
                                tp.post_number = post_number
                            }
                            if let poster = topic["poster"] as? String {
                                tp.poster = poster
                            }
                            if let seen_number = topic["seen_number"] as? String {
                                tp.seen_number = seen_number
                            }
                            if let last_message = topic["last_post_date"] as? String {
                                tp.last_message = last_message
                            }
                            
                            self.topics.append(tp)
                            //print(tp.id)
                            
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.topicTableView.reloadData()
                    })
                    
                }
                SVProgressHUD.dismiss()
                self.items = self.topics.count
            }
            
        }
        
    }
    //MARK: - TAble View Datasource
    
    //TODO: Declare numberOfRowsInSection here:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    
    //TODO: Declare cellForRowAtIndexPath here:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "topicCell", for: indexPath) as! TopicTableViewCell
        
        cell.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        cell.separatorView.backgroundColor = UIColor(red:0.93, green:0.64, blue:0.08, alpha:1.0)
        cell.containerView.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        cell.containerViewDetail.backgroundColor = UIColor(red:0.92, green:0.85, blue:0.76, alpha:1.0)
        let topic = self.topics[indexPath.row]
        cell.name.text = topic.name
        cell.poster.text = topic.poster
        cell.post_number.text = topic.post_number
        cell.seen_number.text = topic.seen_number
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "post") as! PostViewController
        vc.topic_id = self.topics[indexPath.row].id
        vc.forumId = self.forum_id
        vc.topicTitle = self.topics[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    

}
