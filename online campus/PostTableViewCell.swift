//
//  PostTableViewCell.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    
    var yourobj : (() -> Void)? = nil
    @IBOutlet weak var readPost: UIButton!
    @IBOutlet weak var poster: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    var message: String = ""
        
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK : -- Button action
    
    @IBAction func btnAction(sender: UIButton)
    {
        if let btnAction = self.yourobj
        {
            btnAction()
        }
    }
    
    
}
