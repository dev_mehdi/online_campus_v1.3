//
//  testViewController.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class TestViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var testTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //TODO: Set Yourself as Delegate and datasource here:
        testTableView.delegate = self
        testTableView.dataSource = self
        
        //TODO: Register your ForumTableViewCell.xib file here:
        testTableView.register(UINib(nibName:"TestViewCell",bundle: nil), forCellReuseIdentifier: "testCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
    //TODO: Declare cellForRowAtIndexPath here:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath) as! TestViewCell
        cell.title.text = "Title"
        return cell
    }
    
    
    //TODO: Select
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    //TODO: Declare celldidselect
    
    
    
//
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "topicView") as! TopicViewController
//        print(forums[indexPath.row].id)
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
    
    
    
    
    
}
