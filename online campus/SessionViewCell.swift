//
//  SessionViewCell.swift
//  online campus
//
//  Created by mehdi on 23/08/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class SessionViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var viewSession: UIView!
    @IBOutlet weak var StartView: UIView!
    @IBOutlet weak var EndSession: UIView!
    @IBOutlet weak var ViewDate: UIView!
    @IBOutlet weak var session: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    
    
    func customization()  {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
