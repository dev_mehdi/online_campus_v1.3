
import UIKit
import MessageUI

class ContactFormController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var subject: UITextField!
    
    @IBOutlet weak var messageTextField: UITextView!
    
    var departement : String = ""
    
    
    
    var pickerData = ["Choose","IT Departement","Finance"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        picker.dataSource = self
        picker.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submt(_ sender: Any) {
        
        if departement == "Choose" || messageTextField.text == "" || subject.text == nil {
            let sendMailErrorAlert = UIAlertController(title: "Error", message: "Please fill all fields.", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
            sendMailErrorAlert.addAction(dismiss)
            self.present(sendMailErrorAlert, animated: true, completion: nil)
        }else{
            let mailComposeViewController = configureMailComposer()
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            }else{
                showMailError()
            }
        }
        
    }
    //configure MFMail
    func configureMailComposer() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["mehdi@aulm.us"])
        mailComposerVC.setSubject(subject.text!)
        mailComposerVC.setMessageBody("Departement : \(departement) </br> Message : \(messageTextField.text)", isHTML: true)
        return mailComposerVC
    }
    
    //show error send email
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not Send Email", message: "Your Device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    // Picker View
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        departement = pickerData[row]
        print(departement)
    }
    
    

}
