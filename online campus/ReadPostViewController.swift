//
//  ReadPostViewController.swift
//  online campus
//
//  Created by mehdi on 23/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import WebKit
import Ipify
import Alamofire
import SwiftyJSON
import SVProgressHUD
import NotificationBanner


class ReadPostViewController: UIViewController,NotificationBannerDelegate {
    let global = Global()
    var id:String = ""
    var poster:String = ""
    var date:String = ""
    var content:String = ""
    var forumId:String = ""
    var topicId:String = ""
    var created:String = ""
    //outlet
    
    
    @IBOutlet weak var fotterView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var posterLabel: UILabel!
    @IBOutlet weak var postedLabel: UILabel!
    @IBOutlet weak var contentView: UIWebView!
    @IBOutlet weak var textview: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backItem?.title = ""
        headerView.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
        fotterView.backgroundColor = UIColor(red:0.42, green:0.06, blue:0.04, alpha:1.0)
        textview.layer.cornerRadius = 5
        textview.layer.backgroundColor = UIColor(red:0.50, green:0.08, blue:0.07, alpha:1.0).cgColor
        textview.layer.borderWidth = 1
        textview.layer.borderColor = UIColor.white.cgColor
        
        posterLabel.text = poster
        postedLabel.text = date
        let htmlCode = "<html><head><title></title><meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'></head> <body>"+content+"</body></html>"
        contentView.loadHTMLString(htmlCode, baseURL: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    @IBAction func actionReply(_ sender: Any) {
        
        let message:String = textview.text as String
        if message == "" {
            createAlert(title: "Warning!", message: "Please enter your message, thank you!!!")
        }else{
            textview.isEditable = false
            Ipify.getPublicIPAddress { result in
                switch result {
                case .success(let ip):
                    self.addPost(posterIp:ip,message:message)
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
    }
    
    func addPost(posterIp:String,message:String){
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        
        var dbCourse:String = ""
        if  UserDefaults.standard.string(forKey: "dbCourse") != nil {
            dbCourse = UserDefaults.standard.string(forKey: "dbCourse")!
        }
        var firstName:String = ""
        if  UserDefaults.standard.string(forKey: "prenom") != nil {
            firstName = UserDefaults.standard.string(forKey: "prenom")!
        }
        var lastName:String = ""
        if  UserDefaults.standard.string(forKey: "nom") != nil {
            lastName = UserDefaults.standard.string(forKey: "nom")!
        }
        
        let topicId = self.topicId
        let forumId = self.forumId
        print(uid)
        print(isConnected)
        print(message)
        print(dbCourse)
        print(firstName)
        print(lastName)
        print("topicid\(topicId)")
        print(forumId)
        print(posterIp)
        
        let parameters = [
            "isConnected" : isConnected,
            "user_id" : uid,
            "dbCourse": dbCourse,
            "topicId":topicId,
            "forumId":forumId,
            "firstName":firstName,
            "lastName":lastName,
            "posterIp":posterIp,
            "message":message]
        
        let urlString = "\(global.domaine)/claroline/api_mobile/reply.php"
        
        Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print(response)
            if let json = response.result.value  as? NSArray {
                for posts in json{
                    if let post = posts as? NSDictionary {
                        SVProgressHUD.show()
                        if let created = post["create"] as? String {
                            if created == "true" {
                                self.created = created
                                self.textview.isEditable = true
                                self.textview.text = ""
                                showNotificationBanner(bannerStyle: .successMessage, bannerLocation: .Top,
                                                       messageTitle: "Success", messageContent: "Your post successfully added, Thank you!!")
                                
                                
                            }else{
                                showNotificationBanner(bannerStyle: .errorMessage, bannerLocation: .Top,
                                                       messageTitle: "Error", messageContent: "Please make sure you are connected to Internet and try to send a new post, Thank you!!!")

                            }
                            
                        }
                        if self.created == "true" {
                            self.navigationController?.popViewController(animated: true)
                        }
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
        
        
    }
    
    
    // creation alert
    func createAlert(title : String , message : String ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        //creating on Button
        alert.addAction(UIAlertAction(title : "OK",style : UIAlertActionStyle.default,handler :{(action) in alert.dismiss(animated: true, completion: nil)}))
        
        self.present(alert,animated: true,completion: nil)
    }
    
    
    func notificationBannerClick(_ view: NotificationBannerView) {
        dissmissBanner(completion: { Sucess in _ = Bool()
            if(Sucess){
                
            }
        })
    }
    
    
    

    
    
    
    
    
    

}
