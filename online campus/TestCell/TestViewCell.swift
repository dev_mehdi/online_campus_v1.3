//
//  TestViewCell.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class TestViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
