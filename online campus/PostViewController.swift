//
//  PostViewController.swift
//  online campus
//
//  Created by mehdi on 12/07/2018.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import HTMLString
import SVProgressHUD
class PostViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var postTableView: UITableView!
    let global = Global()
    var topicTitle:String = ""
    var topic_id:String = ""
    var forumId:String = ""
    var posts  = [Post]()
    var items = 0
    let titleLabel = UILabel()
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var topicTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backItem?.title = ""
        //TODO: Set Yourself as Delegate and datasource here:
        
        postTableView.delegate = self
        postTableView.dataSource = self
        
        //TODO: Register your ForumTableViewCell.xib file here:
        postTableView.register(UINib(nibName:"PostTableViewCell",bundle: nil), forCellReuseIdentifier: "postCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerView.backgroundColor = UIColor.rbg(r: 92, g: 13, b: 9)
        topicTitleLabel.text = topicTitle
        SVProgressHUD.show()
        postsList()
    }

    
    //MARK: - Fetch List of Posts
    func postsList(){
        posts = []
        var uid : String
        uid = ""
        if  UserDefaults.standard.string(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        }
        var isConnected : String
        isConnected = ""
        if  UserDefaults.standard.string(forKey: "isConnected") != nil {
            isConnected = UserDefaults.standard.string(forKey: "isConnected")!
        }
        
        var dbCourse:String = ""
        if  UserDefaults.standard.string(forKey: "dbCourse") != nil {
            dbCourse = UserDefaults.standard.string(forKey: "dbCourse")!
        }
        if  isConnected == "true" && uid != "" && dbCourse != ""{
            let parameters = ["isConnected" : isConnected,"uid" : uid,"dbCourse": dbCourse,"topicId":self.topic_id]
            let urlString = "\(global.domaine)/claroline/api_mobile/list_post.php"
            
            Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                print(response)
                if let json = response.result.value  as? NSArray {
                    for posts in json{
                        if let post = posts as? NSDictionary {
                            let ps:Post = Post()
                            if let id = post["id"] as? String {
                                ps.id = id
                            }
                            if let poster = post["author"] as? String {
                                ps.poster = poster
                            }
                            if let posted = post["posted"] as? String {
                                ps.posted = posted
                            }
                            if let message = post["post"] as? String {
                                ps.post = message.removingHTMLEntities
                            }
                            self.posts.append(ps)
                            
                            
                        }
                    }
                    OperationQueue.main.addOperation({
                        self.postTableView.reloadData()
                    })
                    SVProgressHUD.dismiss()
                }
                self.items = self.posts.count
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items
    }
    
    
    //TODO: Declare cellForRowAtIndexPath here:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        let post = self.posts[indexPath.row]
        cell.poster.text = post.poster
        cell.date.text = post.posted
        cell.containerView.backgroundColor = UIColor(red:0.42, green:0.06, blue:0.04, alpha:1.0)
        cell.readPost.layer.cornerRadius = 4
        cell.readPost.layer.borderColor = UIColor.white.cgColor
        cell.readPost.layer.borderWidth = 1
        cell.yourobj =
        {
                //Do whatever you want to do when the button is tapped here
            self.openReadPost(index:indexPath.row)
        }
        
        return cell
    }
    
    func openReadPost(index:Int){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "displayPost") as! ReadPostViewController
        if let post:Post = posts[index] {
            vc.poster = post.poster
            vc.date = post.posted
            vc.content = post.post
            vc.id = post.id
            vc.topicId = self.topic_id
            vc.forumId = self.forumId
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   
    
    
}
